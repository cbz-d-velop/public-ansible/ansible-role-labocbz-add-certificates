---
- name: "Get all certs"
  run_once: true
  block:
    - name: "Prepare workspace"
      block:
        - name: "Check if workspace exist"
          register: "import_certificate__tmp_workspace_stat"
          ansible.builtin.stat:
            path: "{{ import_certificate__tmp_workspace }}"

        - name: "Create workspace"
          when: "not import_certificate__tmp_workspace_stat.stat.exists"
          ansible.builtin.file:
            path: "{{ import_certificate__tmp_workspace }}"
            state: "directory"
            recurse: true
            owner: "root"
            group: "root"
            mode: "0755"

    - name: "Get files from sources"
      block:
        - name: "Get bundle from remote source using curl"
          when: "import_certificate__bundle_src is match('http.*://.*')"
          changed_when: false
          ansible.builtin.command: "curl -u '{{ import_certificate__bundle_src_user }}':'{{ import_certificate__bundle_src_password }}' --insecure --max-time 120 -X GET {{ import_certificate__bundle_src }} --output {{ import_certificate__tmp_workspace }}/{{ import_certificate__bundle_name }}" # noqa: command-instead-of-module

        - name: "Get bundle from local source"
          when: "not (import_certificate__bundle_src is match('http.*://.*'))"
          ansible.builtin.copy:
            remote_src: true
            src: "{{ import_certificate__bundle_src }}"
            dest: "{{ import_certificate__tmp_workspace }}"
            owner: "root"
            group: "root"
            mode: "0755"

        - name: "Fetch bundle on controller node (cert)"
          when: "import_certificate__bundle_type == 'cert'"
          ansible.builtin.fetch:
            src: "{{ import_certificate__tmp_workspace }}/{{ import_certificate__bundle_name }}"
            dest: "/tmp/.ansible/add_certificates/{{ import_certificate__bundle_name }}"
            flat: true

        - name: "Fetch bundle on controller node (CA)"
          when: "import_certificate__bundle_type == 'CA'"
          ansible.builtin.fetch:
            src: "{{ import_certificate__tmp_workspace }}/{{ import_certificate__bundle_name }}"
            dest: "/tmp/.ansible/add_certificates/{{ import_certificate__bundle_name }}"
            flat: true

- name: "Deploy bundler on servers"
  when: "import_certificate__bundle_type == 'cert'"
  block:
    - name: "Check if bundle destination exist"
      register: "import_certificate__bundle_dest_stat"
      ansible.builtin.stat:
        path: "{{ import_certificate__bundle_dest }}"

    - name: "Create bundle destination"
      when: "not import_certificate__bundle_dest_stat.stat.exists"
      ansible.builtin.file:
        path: "{{ import_certificate__bundle_dest }}"
        state: "directory"
        recurse: true
        owner: "{{ import_certificate__bundle_dest_user }}"
        group: "{{ import_certificate__bundle_dest_group }}"
        mode: "{{ import_certificate__bundle_dest_mode }}"

    - name: "Import bundle"
      changed_when: false
      ansible.builtin.copy:
        dest: "{{ import_certificate__bundle_dest }}/{{ import_certificate__bundle_name }}"
        src: "/tmp/.ansible/add_certificates/{{ import_certificate__bundle_name }}"
        owner: "{{ import_certificate__bundle_dest_user }}"
        group: "{{ import_certificate__bundle_dest_group }}"
        mode: "{{ import_certificate__bundle_dest_mode }}"

    - name: "Deploy bundle"
      changed_when: false
      ansible.builtin.unarchive:
        dest: "{{ import_certificate__bundle_dest }}"
        src: "{{ import_certificate__bundle_dest }}/{{ import_certificate__bundle_name }}"
        owner: "{{ import_certificate__bundle_dest_user }}"
        group: "{{ import_certificate__bundle_dest_group }}"
        mode: "{{ import_certificate__bundle_dest_mode }}"
        remote_src: true

- name: "Import CA on hosts"
  when: "import_certificate__bundle_type == 'CA'"
  notify: "Update CAs"
  block:
    - name: "Check if bundle destination exist CA on host"
      register: "import_certificate__ca_location_stat"
      ansible.builtin.stat:
        path: "{{ import_certificate__ca_location }}"

    - name: "Create bundle destination CA on host"
      when: "not import_certificate__ca_location_stat.stat.exists"
      ansible.builtin.file:
        path: "{{ import_certificate__ca_location }}"
        state: "directory"
        recurse: true
        owner: "{{ import_certificate__bundle_dest_user }}"
        group: "{{ import_certificate__bundle_dest_group }}"
        mode: "{{ import_certificate__bundle_dest_mode }}"

    - name: "Import bundle CA on host"
      ansible.builtin.copy:
        dest: "{{ import_certificate__ca_location }}/"
        src: "/tmp/.ansible/add_certificates/{{ import_certificate__bundle_name }}"
        owner: "root"
        group: "root"
        mode: "0711"
