# Ansible role: labocbz.import_certificate

![Licence Status](https://img.shields.io/badge/licence-MIT-brightgreen)
![Testing Method](https://img.shields.io/badge/Testing%20Method-Ansible%20Molecule-blueviolet)
![Testing Driver](https://img.shields.io/badge/Testing%20Driver-docker-blueviolet)
![Language Status](https://img.shields.io/badge/language-Ansible-red)
![Compagny](https://img.shields.io/badge/Compagny-Labo--CBZ-blue)
![Author](https://img.shields.io/badge/Author-Lord%20Robin%20Crombez-blue)

## Description

![Tag: Ansible](https://img.shields.io/badge/Tech-Ansible-orange)
![Tag: Debian](https://img.shields.io/badge/Tech-Debian-orange)
![Tag: Ubuntu](https://img.shields.io/badge/Tech-Ubuntu-orange)
![Tag: OpenSSL](https://img.shields.io/badge/Tech-OpenSSL-orange)
![Tag: SSL/TLS](https://img.shields.io/badge/Tech-SSL/TLS-orange)

An Ansible role install a CA your server and/or get cryptographic content.

The Ansible role enables the import of SSL certificate bundles, which are files either in ZIP format or any other archive format, both locally and remotely. When the type of bundle is specified as a CA (Certificate Authority), the role automatically includes it in the cacerts of the target machines, enhancing the security and trust of SSL communications.

In the context of SSL communication, Certificate Authorities play a crucial role in establishing trust between clients and servers. When a certificate is signed by a trusted CA, it verifies the authenticity and integrity of the certificate, enabling secure connections between systems. The role's ability to automatically add the CA bundle to the cacerts ensures that the target machines trust the certificates issued by that particular Certificate Authority.

By seamlessly updating the cacerts with the CA bundle, the role streamlines the process of establishing trust between the target system and the Certificate Authority. This ensures that SSL connections are secured and authenticated, allowing clients to confidently trust the certificates presented by the servers.

In addition to handling CA bundles, the role efficiently manages both local and remote files, performing necessary fetch and placement operations. The inclusion of basic authentication support enhances the security of remote file handling, ensuring that SSL certificate bundles are retrieved securely.

In summary, the Ansible role provides a comprehensive solution for SSL certificate management, offering flexibility in handling various types of bundles, including CAs. The automatic inclusion of CA bundles in the cacerts of target machines streamlines the trust establishment process, ensuring secure and authenticated SSL connections within the environment.

## Usage

```SHELL
# Install husky and init
npm i && npx husky init && npm i validate-branch-name && npm cz

# Initialize the local secrets database, if not already present
MSYS_NO_PATHCONV=1 docker run -it --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest detect-secrets scan --exclude-files 'node_modules' > .secrets.baseline

# Decrypt the local Ansible Vault
# Get the Ansible Local Vault Key on PassBolt and create the .ansible-vault.key file, after that you can unlock your local vault
MSYS_NO_PATHCONV=1 docker run --rm -it -v "$HOME/.docker:/root/.docker" -v "$(pwd):/root/ansible/${DIR}" -w /root/ansible/${DIR} labocbz/ansible-molecule:latest /bin/sh -c 'cat ./.ansible-vault.key > /tmp/.ansible-vault.key && chmod -x /tmp/.ansible-vault.key && ansible-vault decrypt --vault-password-file /tmp/.ansible-vault.key ./tests/inventory/group_vars/vault.yml'
```

### Linters

```SHELL
# Analyse the project for secrets, and audit
./detect-secrets

# Lint Markdown
MSYS_NO_PATHCONV=1 docker run --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest markdownlint './**.md' --disable MD013

# Lint YAML
MSYS_NO_PATHCONV=1 docker run --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest yamllint -c ./.yamllint .

# Lint Ansible
MSYS_NO_PATHCONV=1 docker run --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest ansible-lint --offline --exclude node_modules -x meta-no-info -p .
```

### Local Tests

This repository contain a unique Molecule controller to start containers and tests your roles and playbook inside them.

```SHELL
# Start the Molecule controller
./molecule-controller

# Inside the controller
molecule create
molecule converge
molecule verify
molecule test
```

### Use

In order to use this role, you have to know its defaults vars and their purppose.

### Vars

This role have these default vars here: [defaults.yml](./defaults/main.yml).

### Import and Run

To run this role or use it inside a playbook, import the call task from [converge.yml](./molecule/default/converge.yml).

## Architectural Decisions Records

Here you can put your change to keep a trace of your work and decisions.

### 2023-05-27: First Init

* First init of this role with the bootstrap_role playbook by Lord Robin Crombez

### 2023-06-01: Adding credentials for remote files

* Role handle now remote auth for protected urls

### 2023-06-02: Deploy bundles

* Import/Deploy ZIP bundles on hosts for tests, validation, backup, etc

### 2023-10-06: New CICD, new Images

* New CI/CD scenario name
* Molecule now use remote Docker image by Lord Robin Crombez
* Molecule now use custom Docker image in CI/CD by env vars
* New CICD with needs and optimization

### 2023-02-20: New CI, new Images

* Added support for Ubuntu 22
* Added support for Debian 11/12
* Added new CI
* Refresh vars __

### 2024-05-19: New CI

* Added Markdown lint to the CICD
* Rework all Docker images
* Change CICD vars convention
* New workers
* Removed all automation based on branch

### 2024-10-08: Ansible Vault and automations

* Added one local Ansible Vault
* Edited gitignore file
* Add some commands in documentation

### 2024-12-30: New CICD and images

* Edited all Docker images
* Rework on the CICD
* Enabled SonarQube

### 2025-01-05: Certificates update

* Edit for use the latest version of import_certificates

## Authors

* Lord Robin Crombez

## Sources

* [Ansible role documentation](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_reuse_roles.html)
* [Ansible Molecule documentation](https://molecule.readthedocs.io/)
